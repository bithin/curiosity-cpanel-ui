import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss'],
})
export class DefaultLayoutComponent implements OnInit {
  public navItems = [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'dashboard',
    },
    {
      name: 'Content',
      url: '/content',
      icon: 'dashboard',
      children: [
        {
          name: 'Stories',
          url: '/content/stories',
          icon: 'stories',
        },
        {
          name: 'A.CME',
          url: '/content/acme',
          icon: 'acme',
        }
      ],
    },
    {
      name: 'Campaigns',
      url: '/campaigns',
      icon: 'campaigns',
      children: [
        {
          name: 'History',
          url: '/campaigns/history',
          icon: 'history',
        },
        {
          name: 'Scheduled',
          url: '/campaigns/scheduled',
          icon: 'scheduled',
        },
        {
          name: 'Ticker Update',
          url: '/campaigns/ticker-update',
          icon: 'ticker-update',
        },
      ],
    },
    {
      name: 'Admin',
      url: '/admin',
      icon: 'dashboard',
      children: [
        {
          name: 'Role Manager',
          url: '/role-manager',
          icon: 'role',
        }
      ],
    },
    {
      name: 'Explore',
      url: '/explore',
      icon: 'home_work',
    },
    {
      name: 'Infocenter',
      url: '/infocenter',
      icon: 'info',
    },
    {
      name: 'For You',
      url: '/for-you',
      icon: 'list_alt',
    },
    {
      name: 'MedETV',
      url: '/mede-tv',
      icon: 'live_tv',
      children: [
        {
          name: 'Webinars',
          url: '/webinars',
          icon: 'icon-puzzle',
        },
        {
          name: 'Surgery Suite',
          url: '/surgery-suite',
          icon: 'icon-puzzle',
        },
        {
          name: 'CME & Education',
          url: '/cme-education',
          icon: 'icon-puzzle',
        },
        {
          name: 'Events',
          url: '/events',
          icon: 'icon-puzzle',
        },
        {
          name: 'Animated Case Stories',
          url: '/animated-case-stories',
          icon: 'icon-puzzle',
        },
      ],
    },
    {
      name: 'More',
      url: '/more',
      icon: 'group_work',
      children: [
        {
          name: 'Health Minute',
          url: '/health-minute',
          icon: 'icon-puzzle',
        },
        {
          name: 'COVID-19',
          url: '/covid-19',
          icon: 'icon-puzzle',
        },
        {
          name: 'Guidelines',
          url: '/guidelines',
          icon: 'icon-puzzle',
        },
        {
          name: 'Plexiquiz',
          url: '/plexiquiz',
          icon: 'icon-puzzle',
        },
        {
          name: 'FDA Alerts',
          url: '/fda-alerts',
          icon: 'icon-puzzle',
        },
        {
          name: 'Journal Watch',
          url: '/journal-watch',
          icon: 'icon-puzzle',
        },
      ],
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
