import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-sidebar',
  templateUrl: './page-sidebar.component.html',
  styleUrls: ['./page-sidebar.component.scss']
})
export class PageSidebarComponent implements OnInit {
  @Input() navitem:any;
  constructor() { }

  ngOnInit(): void {
    console.log('navitem',this.navitem);
  }

}
