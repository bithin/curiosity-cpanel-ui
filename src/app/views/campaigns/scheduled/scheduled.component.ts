import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from "../../../services/shared.service";
@Component({
  selector: 'app-scheduled',
  templateUrl: './scheduled.component.html',
  styleUrls: ['./scheduled.component.scss']
})
export class ScheduledComponent implements OnInit {

  constructor(private sharedService: SharedService,
    public router: Router,
    private titleService: Title,
    public activatedRoute: ActivatedRoute) {
    this.titleService.setTitle('Campaigns Schedule | Docplexus Curiosity');
    this.sharedService.updatePageTitle('Campaigns Schedule');
  }

  ngOnInit(): void {
  }

}
