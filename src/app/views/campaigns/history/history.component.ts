import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { WebService } from 'src/app/services/web.service';
import { SharedService } from "../../../services/shared.service";
@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  history:any=[];
  page:number=1;
  constructor(private sharedService: SharedService,
    public router: Router,
    private webService: WebService,
    private titleService: Title,
    public activatedRoute: ActivatedRoute) {
    this.titleService.setTitle('Campaigns History | Docplexus Curiosity');
    this.sharedService.updatePageTitle('Campaigns History');
  }

  ngOnInit(): void {
    this.getHistory();
  }

  getHistory() {
    // this.spinnerService.show();
    let url = `campaign-history?pageNumber=${this.page}`;
    // if (this.filterForm.searchText)
    //   url = url + `&searchText=${this.filterForm.searchText}`;
    this.webService.get(url).subscribe((response: any) => {
      //  this.spinnerService.hide();
      // if (response.status == 1) {
      this.history = response.genres;
      // }

    }, (error) => {
      console.log('error', error);
    });
  }

}
