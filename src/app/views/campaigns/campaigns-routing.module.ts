import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HistoryComponent } from './history/history.component';
import { ScheduledComponent } from './scheduled/scheduled.component';
import { TickerUpdateComponent } from './ticker-update/ticker-update.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'history',
    pathMatch: 'full',
  },
  {
    path: 'history',
    component: HistoryComponent,
    data: {
      title: 'Infocenters | Docplexus Curiosity',
      breadcrumb: [
        {
          label: 'Dashboard',
          url: ''
        },
        {
          label: 'Infocenter',
          url: ''
        }
      ]
    },
  },
  {
    path: 'scheduled',
    component: ScheduledComponent,
    data: {
      title: 'Edit | Docplexus Curiosity',
      breadcrumb: [
        {
          label: 'Dashboard',
          url: 'login'
        },
        {
          label: 'Infocenter',
          url: 'infocenter'
        },
        {
          label: 'Edit',
          url: ''
        }
      ]
    },
  },
  {
    path: 'ticker-update',
    component: TickerUpdateComponent,
    data: {
      title: 'Edit | Docplexus Curiosity',
      breadcrumb: [
        {
          label: 'Dashboard',
          url: 'login'
        },
        {
          label: 'Infocenter',
          url: 'infocenter'
        },
        {
          label: 'Edit',
          url: ''
        }
      ]
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignsRoutingModule { }
