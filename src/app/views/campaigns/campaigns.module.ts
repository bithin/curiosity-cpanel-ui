import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CampaignsRoutingModule } from './campaigns-routing.module';
import { HistoryComponent } from './history/history.component';
import { ScheduledComponent } from './scheduled/scheduled.component';
import { TickerUpdateComponent } from './ticker-update/ticker-update.component';


@NgModule({
  declarations: [
    HistoryComponent,
    ScheduledComponent,
    TickerUpdateComponent
  ],
  imports: [
    CommonModule,
    CampaignsRoutingModule
  ]
})
export class CampaignsModule { }
