import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from "../../../services/shared.service";
@Component({
  selector: 'app-ticker-update',
  templateUrl: './ticker-update.component.html',
  styleUrls: ['./ticker-update.component.scss']
})
export class TickerUpdateComponent implements OnInit {

  constructor(private sharedService: SharedService,
    public router: Router,
    private titleService: Title,
    public activatedRoute: ActivatedRoute) {
    this.titleService.setTitle('Campaigns Ticker | Docplexus Curiosity');
    this.sharedService.updatePageTitle('Campaigns Ticker');
   }

  ngOnInit(): void {
  }

}
