import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoriesComponent } from './stories/stories.component';
import { AcmeComponent } from './acme/acme.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'stories',
    pathMatch: 'full',
  },
  {
    path: 'stories',
    component: StoriesComponent,
    data: {
      title: 'Infocenters | Docplexus Curiosity',
      breadcrumb: [
        {
          label: 'Dashboard',
          url: ''
        },
        {
          label: 'Infocenter',
          url: ''
        }
      ]
    },
  },
  {
    path: 'acme',
    component: AcmeComponent,
    data: {
      title: 'Edit | Docplexus Curiosity',
      breadcrumb: [
        {
          label: 'Dashboard',
          url: 'login'
        },
        {
          label: 'Infocenter',
          url: 'infocenter'
        },
        {
          label: 'Edit',
          url: ''
        }
      ]
    },
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
