import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { WebService } from 'src/app/services/web.service';
import { SharedService } from '../../../services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss'],
})
export class StoriesComponent implements OnInit {
  public storyList: any = [];
  public isEdit: boolean = false;
  public showModal: boolean = false;
  public storyFormDetails: any = {
    contentUUIDList: [],
  };
  constructor(
    private sharedService: SharedService,
    public router: Router,
    private spinnerService: NgxSpinnerService,
    private webService: WebService,
    private titleService: Title,
    private toastr: ToastrService,
    public activatedRoute: ActivatedRoute
  ) {
    this.titleService.setTitle('Content Stories | Docplexus Curiosity');
    this.sharedService.updatePageTitle('Content Stories');
  }

  ngOnInit(): void {
    this.storyFormDetails.contentUUIDList.push({});
    this.getStories();
  }

  getStories() {
    this.spinnerService.show();
    let url = `stories/get-stories`;
    this.webService.get(url).subscribe(
      (response: any) => {
        this.spinnerService.hide();
        this.storyList = response;
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

  openAddEditStoryModal(type: string, obj: any) {

    this.showModal = true;
    if (type == 'edit') {
      this.isEdit = true;
      this.storyFormDetails = { ...obj };
      delete this.storyFormDetails.contentUUIDList;
      let uuids = [...obj.contentUUIDList];
      if (uuids.length > 0) {
        this.storyFormDetails.contentUUIDList = [];
        uuids.forEach((element: any) => {
          this.storyFormDetails.contentUUIDList.push({ id: element });
        });
      }
    } else {
      this.isEdit = false;
      this.storyFormDetails={
        contentUUIDList:[]
      };
      this.storyFormDetails.contentUUIDList.push({});
    }
  }

  addUUID() {
    this.storyFormDetails.contentUUIDList.push({});
  }

  closeModal() {
    this.showModal = false;
  }

  addEditStory(action: string) {
    if (!this.storyFormDetails.title) {
      this.toastr.warning('Please add story title', 'Success!');
      return;
    }
    if (!this.storyFormDetails.thumbnailUrl) {
      this.toastr.warning('Please add story thumbnail url', 'Success!');
      return;
    }

    console.log(this.storyFormDetails);
    let contentUUID: any = [];
    if (this.storyFormDetails.contentUUIDList) {
      this.storyFormDetails.contentUUIDList.forEach((element: any) => {
        if (element.id) {
          contentUUID.push(element.id);
        }
      });
    }
    this.storyFormDetails.contentUUIDs = [];
    this.storyFormDetails.contentUUIDs = contentUUID;
    //  this.spinnerService.show();
    if (action == 'add') {
      let url = `stories/add-stories`;
      this.webService.post(url, this.storyFormDetails).subscribe((response: any) => {
        this.storyFormDetails={};
        this.showModal = false;
        this.toastr.success('Story added successfully', 'Success');
        this.getStories();
      }, (error) => {
        console.log('error', error);
      });
    } else {
      let url = `stories/update-stories?id=${this.storyFormDetails.id}`;
      this.webService.patch(url, this.storyFormDetails).subscribe((response: any) => {
        this.storyFormDetails={};
        this.showModal = false;
        this.toastr.success('Story updated successfully', 'Success');
        this.getStories();
      }, (error:any) => {
        console.log('error', error);
      });
    }

  }
}
