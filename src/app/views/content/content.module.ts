import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { ContentRoutingModule } from './content-routing.module';
import { AcmeComponent } from './acme/acme.component';
import { StoriesComponent } from './stories/stories.component';


@NgModule({
  declarations: [
    AcmeComponent,
    StoriesComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ContentRoutingModule
  ]
})
export class ContentModule { }
