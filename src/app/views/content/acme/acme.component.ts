import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { WebService } from 'src/app/services/web.service';
import { SharedService } from '../../../services/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-acme',
  templateUrl: './acme.component.html',
  styleUrls: ['./acme.component.scss']
})
export class AcmeComponent implements OnInit {
  public acmeList: any = [];
  public isEdit: boolean = false;
  public showModal: boolean = false;
  public storyFormDetails: any = {
    contentUUIDList: [],
  };
  constructor( private sharedService: SharedService,
    public router: Router,
    private spinnerService: NgxSpinnerService,
    private webService: WebService,
    private titleService: Title,
    private toastr: ToastrService,
    public activatedRoute: ActivatedRoute) {
    this.titleService.setTitle('Content A.CME | Docplexus Curiosity');
    this.sharedService.updatePageTitle('Content A.CME');
   }

  ngOnInit(): void {
    this.getACMEListing();
  }

  getACMEListing(){
    this.spinnerService.show();
    let url = `acme/get-acme-listing`;
    this.webService.get(url).subscribe(
      (response: any) => {
        this.spinnerService.hide();
        this.acmeList = response;
      },
      (error) => {
        console.log('error', error);
      }
    );
  }

}
