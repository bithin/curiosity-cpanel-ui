import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from "../../../services/shared.service";
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private sharedService: SharedService,
    public router: Router,
    private titleService: Title,
    public activatedRoute: ActivatedRoute) {
      this.titleService.setTitle('Infocenters | Docplexus Curiosity');
      this.sharedService.updatePageTitle('Infocenter');
  }

  ngOnInit(): void {
  }

}
