import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(private sharedService: SharedService,
    public router: Router,
    private titleService: Title,
    public activatedRoute: ActivatedRoute) {
      this.titleService.setTitle('Edit | Docplexus Curiosity');
      this.sharedService.updatePageTitle('KH3d2 - Sanofi Cardio Connect');
  }

  ngOnInit(): void {
  }

}
