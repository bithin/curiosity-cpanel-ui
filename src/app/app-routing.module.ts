import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './default-layout/default-layout.component';

import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: "profile",
        loadChildren: () =>
          import("./views/profile/profile.module").then(
            (m) => m.ProfileModule
          ),
      },
      {
        path: "infocenter",
        loadChildren: () =>
          import("./views/infocenter/infocenter.module").then(
            (m) => m.InfocenterModule
          ),
      },
      {
        path: "content",
        loadChildren: () =>
          import("./views/content/content.module").then(
            (m) => m.ContentModule
          ),
      },
      {
        path: "campaigns",
        loadChildren: () =>
          import("./views/campaigns/campaigns.module").then(
            (m) => m.CampaignsModule
          ),
      }
    ]
  },
 // { path: '**', component: P404Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
