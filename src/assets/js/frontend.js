// // get exact viewport height on window resize
// window.addEventListener('resize', () => {
// 	let vh = window.innerHeight * 0.01;
// 	document.documentElement.style.setProperty('--vh', `${vh}px`);
// });

// -------------------------------------------

$(function () {
  "use script";

  const sb = new PerfectScrollbar(".sidebar-body", {
    suppressScrollX: true,
  });

  $(".sidebar").on("mouseenter mouseleave", function (e) {
    var isHover = e.type === "mouseenter" ? true : false;

    if ($(".sidebar").hasClass("minimized")) {
      if (isHover) {
        setTimeout(function () {
          $(".sidebar").addClass("expand");
          sb.update();
        });
      } else {
        $(".sidebar").removeClass("expand");
        $(".sidebar-body").scrollTop(0);
        sb.update();
      }
    }
  });

  $(".search-body .form-control").on("focusin focusout", function (e) {
    $(this).parent().removeClass("onhover");

    if (e.type === "focusin") {
      $(this).parent().addClass("onfocus");
    } else {
      $(this).parent().removeClass("onfocus");
    }
  });

  $(".search-body").on("mouseover mouseleave", function (e) {
    if (!$(this).hasClass("onfocus")) {
      $(this).toggleClass("onhover", e.type === "mouseover");
    }
  });

  // single level menu
  $(".nav-sidebar > .nav-link").on("click", function (e) {
    e.preventDefault();

    // remove active siblings
    $(this).addClass("active").siblings().removeClass("active");

    // remove active siblings from other nav
    var ss = $(this).closest(".nav-sidebar").siblings(".nav-sidebar");
    var sg = $(this).closest(".nav-group").siblings(".nav-group");

    ss.find(".active").removeClass("active");
    ss.find(".show").removeClass("show");

    sg.find(".active").removeClass("active");
    sg.find(".show").removeClass("show");
  });

  // two level menu
  $(".nav-sidebar .nav-item").on("click", ".nav-link", function (e) {
    e.preventDefault();

    if ($(this).hasClass("with-sub")) {
      $(this).parent().toggleClass("show");
      $(this).parent().siblings().removeClass("show");
    } else {
      $(this).parent().addClass("active").siblings().removeClass("active");
      $(this).parent().siblings().find(".sub-link").removeClass("active");
    }

    var ss = $(this).closest(".nav-sidebar").siblings(".nav-sidebar");
    var sg = $(this).closest(".nav-group").siblings(".nav-group");

    ss.find(".active").removeClass("active");
    ss.find(".show").removeClass("show");

    sg.find(".active").removeClass("active");
    sg.find(".show").removeClass("show");

    sb.update();
  });

  $(".nav-sub").on("click", ".sub-link", function (e) {
    e.preventDefault();

    $(this).addClass("active").siblings().removeClass("active");

    $(this)
      .closest(".nav-item")
      .addClass("active")
      .siblings()
      .removeClass("active");
    $(this)
      .closest(".nav-item")
      .siblings()
      .find(".sub-link")
      .removeClass("active");

    $(this)
      .closest(".nav-sidebar")
      .siblings()
      .find(".active")
      .removeClass("active");
    $(this)
      .closest(".nav-group")
      .siblings()
      .find(".active")
      .removeClass("active");
  });

  $(".nav-group-label").on("click", function () {
    $(this).closest(".nav-group").toggleClass("show");
    $(this).closest(".nav-group").siblings().removeClass("show");

    sb.update();
  });

  // content menu
  $("#contentMenu").on("click", function (e) {
    e.preventDefault();
    $(".sidebar").toggleClass("minimized");

    $(".sidebar-body").scrollTop(0);
    sb.update();
  });

  // mobile menu
  $("#mobileMenu").on("click", function (e) {
    e.preventDefault();
    $("body").toggleClass("sidebar-show");
  });

  // for demo only
  $("#navigationSkins a").on("click", function (e) {
    e.preventDefault();

    var s = $(this).attr("data-skin");
    localStorage.setItem("skin", s);

    location.reload();
  });

  $("#navigationStyles a").on("click", function (e) {
    e.preventDefault();

    var s = $(this).attr("data-style");
    localStorage.setItem("style", s);

    location.reload();
  });

  //on page load
  var sk = localStorage.getItem("skin") ? localStorage.getItem("skin") : "base";
  var st = localStorage.getItem("style")
    ? localStorage.getItem("style")
    : "base";

  // skin
  $("body").attr("class", function (i, c) {
    return c.replace(/(^|\s)skin-\S+/g, "");
  });

  $("body").addClass("skin-" + sk);
  $("#navigationSkins a[data-skin=" + sk + "]")
    .addClass("active")
    .siblings()
    .removeClass("active");

  //sidebar nav style
  $(".nav-sidebar").attr("class", function (i, c) {
    return c.replace(/(^|\s)style-\S+/g, "");
  });

  if (st !== "base") {
    $(".nav-sidebar").addClass("style-" + st);
  }

  $("#navigationStyles a[data-style=" + st + "]")
    .addClass("active")
    .siblings()
    .removeClass("active");

	// Tab //

	$(".tab-1 a").click(function () {
		// Check for active
		$(".tab-1 li").removeClass("active");
		$(this).parent().addClass("active");
	
		// Display active tab
		let currentTab = $(this).attr("href");
		$(".tab-content .edit-content").hide();
		$(currentTab).show();
	
		return false;
	});


// Image Cropper //

$.fn.simpleCropper = function (onComplete) {

  var image_dimension_x = 600;
  var image_dimension_y = 600;
  var scaled_width = 0;
  var scaled_height = 0;
  var x1 = 0;
  var y1 = 0;
  var x2 = 0;
  var y2 = 0;
  var current_image = null;
  var image_filename = null;
  var aspX = 1;
  var aspY = 1;
  var file_display_area = null;
  var ias = null;
  var original_data = null;
  var jcrop_api;
  var bottom_html = "<input type='file' id='fileInput' name='files[]'/ accept='image/*'><canvas id='myCanvas' style='display:none;'></canvas><div id='modal'></div><div id='preview'><div class='buttons'><div class='cancel'></div><div class='ok'></div></div></div>";
  $('body').append(bottom_html);

  //add click to element
  this.click(function () {
      aspX = $(this).width();
      aspY = $(this).height();
      file_display_area = $(this);
      $('#fileInput').click();
  });

  $(document).ready(function () {
      //capture selected filename
      $('#fileInput').change(function (click) {
          imageUpload($('#preview').get(0));
          // Reset input value
          $(this).val("");
      });

      //ok listener
      $('.ok').click(function () {
          preview();
          $('#preview').delay(100).hide();
          $('#modal').hide();
          jcrop_api.destroy();
          reset();
      });

      //cancel listener
      jQuery('.cancel').click(function (event) {
          $('#preview').delay(100).hide();
          $('#modal').hide();
          jcrop_api.destroy();
          reset();
      });
      jQuery(".form-field").each(function(){
        if (jQuery(this).val()) {
          jQuery(this)
            .parent(".form-element, .form-element-2")
            .addClass("has-value");
        }
      });
      
  });

  function reset() {
      scaled_width = 0;
      scaled_height = 0;
      x1 = 0;
      y1 = 0;
      x2 = 0;
      y2 = 0;
      current_image = null;
      image_filename = null;
      original_data = null;
      aspX = 1;
      aspY = 1;
      file_display_area = null;
  }

  function imageUpload(dropbox) {
      var file = $("#fileInput").get(0).files[0];

      var imageType = /image.*/;

      if (file.type.match(imageType)) {
          var reader = new FileReader();
          image_filename = file.name;

          reader.onload = function (e) {
              // Clear the current image.
              $('#photo').remove();

              original_data = reader.result;

              // Create a new image with image crop functionality
              current_image = new Image();
              current_image.src = reader.result;
              current_image.id = "photo";
              current_image.style['maxWidth'] = image_dimension_x + 'px';
              current_image.style['maxHeight'] = image_dimension_y + 'px';
              current_image.onload = function () {
                  // Calculate scaled image dimensions
                  if (current_image.width > image_dimension_x || current_image.height > image_dimension_y) {
                      if (current_image.width > current_image.height) {
                          scaled_width = image_dimension_x;
                          scaled_height = image_dimension_x * current_image.height / current_image.width;
                      }
                      if (current_image.width < current_image.height) {
                          scaled_height = image_dimension_y;
                          scaled_width = image_dimension_y * current_image.width / current_image.height;
                      }
                      if (current_image.width == current_image.height) {
                          scaled_width = image_dimension_x;
                          scaled_height = image_dimension_y;
                      }
                  }
                  else {
                      scaled_width = current_image.width;
                      scaled_height = current_image.height;
                  }

                  // set the image size to the scaled proportions which is required for at least IE11
                  current_image.style['width'] = scaled_width + 'px';
                  current_image.style['height'] = scaled_height + 'px';

                  // Position the modal div to the center of the screen
                  $('#modal').css('display', 'block');
                  var window_width = $(window).width() / 2 - scaled_width / 2 + "px";
                  var window_height = $(window).height() / 2 - scaled_height / 2 + "px";

                  // Show image in modal view
                  $("#preview").css("top", window_height);
                  $("#preview").css("left", window_width);
                  $('#preview').show(500);


                  // Calculate selection rect
                  var selection_width = 0;
                  var selection_height = 0;

                  var max_x = Math.floor(scaled_height * aspX / aspY);
                  var max_y = Math.floor(scaled_width * aspY / aspX);


                  if (max_x > scaled_width) {
                      selection_width = scaled_width;
                      selection_height = max_y;
                  }
                  else {
                      selection_width = max_x;
                      selection_height = scaled_height;
                  }

                  ias = $(this).Jcrop({
                      onSelect: showCoords,
                      onChange: showCoords,
                      bgColor: '#747474',
                      bgOpacity: .4,
                      aspectRatio: aspX / aspY,
                      setSelect: [0, 0, selection_width, selection_height]
                  }, function () {
                      jcrop_api = this;
                  });
              }

              // Add image to dropbox element
              dropbox.appendChild(current_image);
          }

          reader.readAsDataURL(file);
      } else {
          dropbox.innerHTML = "File not supported!";
      }
  }

  function showCoords(c) {
      x1 = c.x;
      y1 = c.y;
      x2 = c.x2;
      y2 = c.y2;
  }

  function preview() {
      // Set canvas
      var canvas = document.getElementById('myCanvas');
      var context = canvas.getContext('2d');

      // Delete previous image on canvas
      context.clearRect(0, 0, canvas.width, canvas.height);

      // Set selection width and height
      var sw = x2 - x1;
      var sh = y2 - y1;


      // Set image original width and height
      var imgWidth = current_image.naturalWidth;
      var imgHeight = current_image.naturalHeight;

      // Set selection koeficient
      var kw = imgWidth / $("#preview").width();
      var kh = imgHeight / $("#preview").height();

      // Set canvas width and height and draw selection on it
      canvas.width = aspX;
      canvas.height = aspY;
      context.drawImage(current_image, (x1 * kw), (y1 * kh), (sw * kw), (sh * kh), 0, 0, aspX, aspY);

      // Convert canvas image to normal img
      var dataUrl = canvas.toDataURL();
      var imageFoo = document.createElement('img');
      imageFoo.src = dataUrl;

      // Append it to the body element
      $('#preview').delay(100).hide();
      $('#modal').hide();
      file_display_area.html('');
      file_display_area.append(imageFoo);

      if (onComplete) onComplete(
          {                    
              "original": { "filename": image_filename, "base64": original_data, "width": current_image.width, "height": current_image.height },
              "crop": { "x": (x1 * kw), "y": (y1 * kh), "width": (sw * kw), "height": (sh * kh) }
          }
         );
  }

  $(window).resize(function () {
      // Position the modal div to the center of the screen
      var window_width = $(window).width() / 2 - scaled_width / 2 + "px";
      var window_height = $(window).height() / 2 - scaled_height / 2 + "px";

      // Show image in modal view
      $("#preview").css("top", window_height);
      $("#preview").css("left", window_width);
  });
}
$('.cropme').simpleCropper();

});

//Dropdowns
jQuery("body").on("click", ".ui-dropdown .ui-dropdown--selected", function (e) {
  jQuery(".ui-dropdown.is-open").removeClass("is-open");
  jQuery(this).parent(".ui-dropdown").toggleClass("is-open");
});

jQuery("body").on(
  "click",
  ".ui-dropdown .ui-dropdown--items > a",
  function (e) {
    jQuery(this).parents(".ui-dropdown").removeClass("is-open");
    jQuery(this)
      .parents(".ui-dropdown")
      .find(".ui-dropdown--selected")
      .text(jQuery(this).attr("data-val"));
    jQuery(this).parents(".ui-dropdown").addClass("value-selected");
  }
);

jQuery("body").on(
  "click",
  ".ui-dropdown .ui-dropdown--items .filter-box a",
  function (e) {
    jQuery(this).parents(".ui-dropdown").removeClass("is-open");
    jQuery(this)
      .parents(".ui-dropdown")
      .find(".ui-dropdown--selected")
      .text(jQuery(this).attr("data-val"));
    jQuery(this).parents(".ui-dropdown").addClass("value-selected");
  }
);

jQuery("body").on("click", ".ui-dropdown .ui-dropdown--reset", function (e) {
  jQuery(this).parent(".ui-dropdown").removeClass("value-selected");
  let labl = jQuery(this).attr("data-text")
    ? jQuery(this).attr("data-text")
    : "";
  jQuery(this)
    .parents(".ui-dropdown")
    .find(".ui-dropdown--selected")
    .text(labl);
});

// add has value if input has value
jQuery("body").on("focus", ".form-field", function (e) {
  jQuery(this).parent(".form-element, .form-element-2").addClass("has-value");
  localStorage.setItem("has-value", $(this).parent().index());
});

jQuery("body").on("blur", ".form-field", function (e) {
  if (!jQuery(this).val()) {
    jQuery(this)
      .parent(".form-element, .form-element-2")
      .removeClass("has-value");
  }
});




// modal
const openModal = (whichModal) => {
  // close all open modal at first
  let openModals = document.querySelectorAll(".sds-modal");
  Array.from(openModals).forEach(function (openModal) {
    openModal.classList.remove("is-active");
  });
  // target modal
  let targetModal = document.querySelector(`#${whichModal}`);
  // open target modal
  document.body.classList.add("bound");
  targetModal.classList.add("is-active");
  // exit target modal
  let exitModal = document.querySelectorAll(".sds-modal-exit");
  for (let i = 0; i < exitModal.length; i++) {
    exitModal[i].addEventListener("click", function () {
      document.body.classList.remove("bound");
      targetModal.classList.remove("is-active");
      // in case video modal
      if (whichModal == "modal--vdo") {
        targetModal.querySelector("iframe").setAttribute("src", "");
      }
    });
  }
};


